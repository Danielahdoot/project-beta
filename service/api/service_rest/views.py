from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_id",
        "first_name",
        "last_name",
        "id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "customer",
        "technician",
        "vin",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):

    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians":technicians},
            encoder=TechnicianEncoder,

        )
    else:
        content = json.loads(request.body)
        technicians = Technician.objects.create(**content)
        return JsonResponse(
            technicians,
            encoder=TechnicianEncoder,
            safe=False,
        )
@require_http_methods(["GET", "DELETE"])
def api_detail_technicians(request, pk):
    if request.method == "GET":
        technician = Technician.objects.filter(id=pk)
        return JsonResponse(
            technician, encoder=TechnicianEncoder, safe=False
        )

    #delete
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()

        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentEncoder,
        )

    else:
        content = json.loads(request.body)


        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(employee_id = technician_id)
            content["technician"] = technician

        except  Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician Employee Id"},
                status = 400
            )

        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentEncoder,
            safe=False,
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_appointments(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.filter(id=pk)
        return JsonResponse(
            appointment, encoder=AppointmentEncoder, safe=False
        )


    elif request.method == "PUT" and request.path.endswith("/cancel"):
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "Cancelled"
        appointment.save()
        return JsonResponse({"message": "Appointment cancelled"})


    elif request.method == "PUT" and request.path.endswith("/finish"):
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "Finished"
        appointment.save()
        return JsonResponse({"message": "Appointment Finished"})

    #delete
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["GET"])
def api_list_automobileVO(request):
    if request.method == "GET":
        automobileVO = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobileVO":automobileVO},
            encoder=AutomobileVOEncoder,
        )
