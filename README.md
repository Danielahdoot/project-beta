# CarCar

Team:

* Person 1 - Daniel Ahdoot (Automobile Sales Microservice)
* Person 2 - Andrew Lam (Automobile Service Microservice)

## Design
CarCar is a web application that helps automobile dealerships manage their inventory, sales, and service. It is made up of three microservices: inventory, sales, and service. These microservices use RESTful APIs in the back-end to provide data to the user interface on the front-end. The sales and service microservices each have their own Automobile value object (AutomobileVO), which is created and updated through their own polling application that requests and gets Automobile data from the Inventory.
Docker is used to host the three microservices that make up the CarCar application, with each microservice running in its own Docker container.

![Alt text](Drawing.png)

## Installation
1. Open up your terminal
2. Clone the repository
```
git clone https://gitlab.com/Danielahdoot/project-beta
```
3. Change your working directory to the project's directory
```
cd project-beta
```
4. Open up Docker Desktop and run the following commands in your terminal:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
5. Access site via http://localhost:3000

## How to Use

### To create a vehicle:

A manufacturer is required to create a model and a model is required to create an automobile.

1. Begin by creating a manufacturer from the inventory dropdown.
2. Create a model from the inventory dropdown.
3. Create automobile from the inventory dropdown.
4. You can view the items you created in each of their respective lists.

### To sell vehicles:

A customer, salesperson and a vehicle are required to create a sale.

1. Begin by creating a customer from the sales dropdown.
2. Create a salesperson from the sales dropdown.
3. Sell a vehicle from the sales drop down.
4. Get a list of all vehicles sold from the Sales dropdown.
5. Get salesperson recrods from the sales dropdown.

### To schedule a service appointment

A customer, technician and a vehicle are required to create a service appointment.

1. Being by creating a technician in "Add a technican" form on the Navbar.
2. Go to "Create a Service Appintment" to fill out the form, enter VIN#, Customer Name, select a date, select a time, select a technician, and enter a reason for service.
3. After filling out the form, press the create button to complete.
4. A green notification popup on the lower right corner should say the service appointment is successfully created if there are no errors.
5. To check the appointmment is successfully added manually, go to Service Appointment on the Navbar to check if the appointment is added to the list.



## Service microservice

**Overview**

The Service Microservice is a collection of two applications, api and poll. These two applications share a database. The Service Microservice is a bounded context, which means that it is a self-contained unit of functionality. It contains all of the CarCar dealership's functionality and data surrounding service appointments and technicians.

### Service Microservice integration With Inventory Microservice:
The Automobile Service microservice polls the vehicle identification number (VIN) from the automobiles in inventory to an Automobile Value Object (VO) model. The Automobile VO is a permanent object that records all past or existing automobile inventory, regardless of sales status. By matching the VIN of the car that is receiving service, we can determine which car can receive VIP service.

### Models For The Service Microservice:

- AutomobileVO
	-vin (Character Field, unique value)

- Appointment
	- date_time (DateTime Field)
	- reason (Character Field)
	- status (default value = Created)
	- vin (Character Field)
	- customer (Character Field)
	- technician (foreignkey to Model Technican)

- Technician
   -first_name (Character Field)
   -last_name (Character Field)
   -employee_id = (Numeric value, unique value)


### The Service Microservice Restful APIs

# AutomobileVO

- List AutmobileVO	GET	http://localhost:8080/api/automobileVO/

<details>
<summary><strong>Example GET Outputs for AutomobileVO</strong></summary>

```
{
	"automobileVO": [
		{
			"vin": "2HKRL1851XH526025",
			"import_href": "/api/automobiles/2HKRL1851XH526025/"
		},
		{
			"vin": "1GBJ7D1B4BV132373",
			"import_href": "/api/automobiles/1GBJ7D1B4BV132373/"
		},
	]
}
```
</details>

# Appointment

- List appointment	GET	http://localhost:8080/api/appointments/
- Create an appointment	POST	http://localhost:8080/api/appointments/
- Update an appoinement status to Cancel	PUT	http://localhost:8080/api/appointments/id/cancel
- Update an appoinement status to Finish	DELETE	http://localhost:8080/api/appointments/id/finish

<details>
<summary><strong>Example GET Outputs for
Appointment</strong></summary>

```
		{
			"date_time": "2023-04-17T18:00:00+00:00",
			"reason": "engine check",
			"status": "Cancelled",
			"customer": "Tom",
			"technician": {
				"employee_id": 12345,
				"first_name": "Daniel",
				"last_name": "Ahdoot (Boss)",
				"id": 1
			},
			"vin": "1GBJ7D1B4BV132373",
			"id": 4
		},
		{
			"date_time": "2023-04-12T21:30:32+00:00",
			"reason": "tire check",
			"status": "Cancelled",
			"customer": "Paul",
			"technician": {
				"employee_id": 12333,
				"first_name": "Andrew",
				"last_name": "Lam",
				"id": 3
			},
			"vin": "2HKRL1851XH526025",
			"id": 3
		},
```
</details>

<details>
<summary><strong>Example POST Input for
Appointment</strong></summary>

```
	{
	"date_time": "2023-04-12T21:30:32+00:00",
	"reason": "tire check",
	"status": "Created",
	"customer": "Paul",
	"technician": {
		"employee_id": 12333,
		"first_name": "Andrew",
		"last_name": "Lam",
		"id": 3
	},
	"vin": "1GBJ7D1B4BV132373",
	"id": 6
}
```
</details>

# Technician

- List technician	GET	http://localhost:8080/api/technicians/
- Create a technician	POST http://localhost:8080/api/technicians/


<details>
<summary><strong>Example GET Outputs for
Technician</strong></summary>

```
		{
			"employee_id": 12345,
			"first_name": "Daniel",
			"last_name": "Ah",
			"id": 1
		},
		{
			"employee_id": 12333,
			"first_name": "Andrew",
			"last_name": "Lee",
			"id": 3
		},
```
</details>

<details>
<summary><strong>Example POST Input for
Technician</strong></summary>

```
	{
	"employee_id": 12333,
	"first_name": "Andrew",
	"last_name": "Lee",
	"id": 1
}
```
</details>

## Inventory Insomnia URL, Port, JSON Requirements

# Manufacturers:

- List manufacturers	GET	http://localhost:8100/api/manufacturers/
- Create a manufacturer	POST	http://localhost:8100/api/manufacturers/
- Get a specific manufacturer	GET	http://localhost:8100/api/manufacturers/:id/
- Update a specific manufacturer	PUT	http://localhost:8100/api/manufacturers/:id/
- Delete a specific manufacturer	DELETE	http://localhost:8100/api/manufacturers/:id/

- Get Manufacturers http://localhost:8100/api/manufacturers/
- Create Manufacturers http://localhost:8100/api/manufacturers/
- JSON:

```
{
  "name": "Chrysler"
}
```



# Vehicle Models:

- List vehicle models	GET	http://localhost:8100/api/models/
- Create a vehicle model	POST	http://localhost:8100/api/models/
- Get a specific vehicle model	GET	http://localhost:8100/api/models/:id/
- Update a specific vehicle model	PUT	http://localhost:8100/api/models/:id/
- Delete a specific vehicle model	DELETE	http://localhost:8100/api/models/:id/

- Get vehicle models http://localhost:8100/api/models/
- Create vehicle model http://localhost:8100/api/models/
- JSON:

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or the picture URL. It is not possible to update a vehicle model's manufacturer.

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```



# Automobile Information:

- List automobiles	GET	http://localhost:8100/api/automobiles/
- Create an automobile	POST	http://localhost:8100/api/automobiles/
- Get a specific automobile	GET	http://localhost:8100/api/automobiles/:vin/
- Update a specific automobile	PUT	http://localhost:8100/api/automobiles/:vin/
- Delete a specific automobile	DELETE	http://localhost:8100/api/automobiles/:vin/

You can create an automobile with its color, year, VIN, and the id of the vehicle model.
- Get Automobiles http://localhost:8100/api/automobiles/
- Create Automobile http://localhost:8100/api/automobiles/
- JSON:

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```


You can update the color, year, and sold status of an automobile.

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```



# Sales Information

- List salespeople	GET	http://localhost:8090/api/salespeople/
- Create a salespeople	POST	http://localhost:8090/api/salespeople/
- Delete a specific salespeople	DELETE	http://localhost:8090/api/salespeople/:id
- List customer	GET	http://localhost:8090/api/customers/
- Create a customer	POST	http://localhost:8090/api/customers/
- Delete a specific customer	DELETE	http://localhost:8090/api/customers/:id
- List sales	GET	http://localhost:8090/api/sales/
- Create a sale	POST	http://localhost:8090/api/sales/
- Delete a sale	DELETE	http://localhost:8090/api/sales/:id

- Get Salespeople http://localhost:8090/api/salespeople/
- Create Salespeople http://localhost:8090/api/salespeople/
- Get Customer http://localhost:8090/api/customers/
- Create Customer http://localhost:8090/api/customers/
- Get Sale http://localhost:8090/api/sales/
- Create Sale http://localhost:8090/api/sales/

Creating Sales people requires first name last name and employee ID
```
{
			"first_name": "dan",
			"last_name": "Ban",
			"employee_id": "2",
},
```

To get the details or delete for the car you must know the ID of the car. This is an example of how the detail look

```
{
	"first_name": "dan",
	"last_name": "Ban",
	"employee_id": "2",
	"id": 1
}
```

To Create a customer you need the first name, last name, adress and phone number

```
{
	"first_name": "dan",
	"last_name": "ahd",
	"address": "11692 CHENT ST",
	"phone_number": "3333333",
}
```

To Create a sale you will need customer, automobile, salesperson, price.

```
{
  "automobile": "VIN #",
  "salesperson": "Salesperson ID",
  "customer": "customer ID",
  "price": "price"
}

```

For Sales List the information should look like this

```
{
	"customer": {
		"first_name": "dan",
		"last_name": "ahd",
		"address": "11692 CHENAULT ST",
		"phone_number": "3333333",
		"id": 1
	},
	"automobile": {
		"vin": "2C3CCACG5CH278245",
		"id": 1,
		"sold": false
	},
	"salesperson": {
		"first_name": "fda",
		"last_name": "Ban",
		"employee_id": "22",
		"id": 2
	},
	"price": 112223,
	"id": 1
}
```
