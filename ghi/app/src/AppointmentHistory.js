import React, { useState, useEffect } from 'react';

function AppointmentHistory({ appointments, getAutomobileVOs, automobileVOs}) {

    /* Split appointment dateTime into two separate variables object */
    const formatDateTime = (dateTimeString) => {
        const appointmentDateTime = new Date(dateTimeString);
        const formattedDate = appointmentDateTime.toLocaleDateString();
        const formattedTime = appointmentDateTime.toLocaleTimeString();
        return { formattedDate, formattedTime };
    };


    /* Initalize Search field and filter's State */
    const [searchTerm, setSearchTerm] = useState('');
    const [filteredAppointments, setFilteredAppointments] = useState(appointments);

    /* Handle filter operation when Search button is pressed */
    const filterAppointments = () => {
        const newFilteredAppointments = appointments.filter(appointment => {
            return appointment.vin.toLowerCase().includes(searchTerm.toLowerCase());
        });
        setFilteredAppointments(newFilteredAppointments);
    };

    /* Reset the filter */
    const resetFilter = () => {
        setFilteredAppointments(appointments);
        setSearchTerm('');
    };

    useEffect(() => { getAutomobileVOs(); }, []);

    return (
        <div>
            <h1>Service History</h1>

            { /* Search bar components */}
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <input
                    className="form-control"
                    type="text"
                    placeholder="Search by VIN..."
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                    style={{ width: 'calc(30ch)', marginLeft: '3px' }}
                />
                <button
                    className="btn btn-primary"
                    type="button"
                    onClick={filterAppointments}
                    style={{ marginLeft: '8px' }}
                >
                    Search
                </button>
                <button
                    className="btn btn-outline-primary"
                    type="button"
                    onClick={resetFilter}
                    style={{ marginLeft: '8px' }}
                >
                    Reset
                </button>
            </div>

            <p></p>


            {/* Table components */}
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                        {/* Return No Matching VIN # when no matches */}
                    {filteredAppointments.length === 0 ? (
                        <tr>
                            <td colSpan="10">No Matching VIN #</td>
                        </tr>
                    ) :
                        (
                            // Main filter component
                            filteredAppointments.map((appointment) => {

                                const { formattedDate, formattedTime } = formatDateTime(appointment.date_time);  //split dateTime into Date and Time variables

                                const isVIP = automobileVOs.find(auto => auto.vin === appointment.vin) ? "Yes" : "No"; //Search matching automobiles VIN to return is VIP?

                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{isVIP}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{formattedDate}</td>
                                        <td>{formattedTime}</td>
                                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                    </tr>
                                );
                            })
                        )}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentHistory;
